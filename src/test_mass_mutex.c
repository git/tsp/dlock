/*
    test_mass_mutex.c - dlock uninstrumented sample program
    Copyright (C) 2006,2007  Frederik Deweerdt <frederik.deweerdt@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

#define NB_MUTEXES 500
#define NB_LOOPS 250

static pthread_mutex_t *mutexes;

static void *f(void *unused)
{
        int i;
        int begin = random() % (NB_MUTEXES / 2);
        int end = (random() % (NB_MUTEXES / 2)) + (NB_MUTEXES / 2);

        for (i = begin; i < end; i++) {
            pthread_mutex_init(&mutexes[i], NULL);
        }
        for (i = begin; i < end; i++) {
            pthread_mutex_lock(&mutexes[i]);
        }
        for (i = (end - 1); i >= begin; i--) {
            pthread_mutex_unlock(&mutexes[i]);
        }

	return NULL;
}

int main(void)
{
        pthread_t t;

        mutexes = calloc(NB_MUTEXES, sizeof(*mutexes));
        if (!mutexes) {
                perror("calloc");
                exit(-1);
        }

        pthread_create(&t, NULL, f, NULL);
        pthread_join(t, NULL);

        return 0;
}
