/*
    dlock.c - dlock core code
    Copyright (C) 2006,2007  Frederik Deweerdt <frederik.deweerdt@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#if defined(__linux__)
#include <sys/queue.h>
#else
#include "queue.h"
#endif


#include <dlfcn.h>
#include <execinfo.h>
#include <pthread.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "dlock_core.h"

#if defined (__sun)
typedef void (*sighandler_t)(int);
#endif


#include "dlock.h"
#include "dlock_core.h"
#include "pthread_spinlocks.h"
#include "pthread_mutexes.h"
#include "tree.h"

/** Log file, stderr by default */
static FILE *dlock_log_file;

static struct lock_desc *get_lock_desc(dlock_lock_t *lock);

/*
 * get a fast time source for give architecture
 */
#if defined(__i386__)
static __inline__ unsigned long long int arch_get_ticks(void)
{
	unsigned long long int x;
	asm volatile ("rdtsc" : "=A" (x));
	return x;
}
#elif defined(__x86_64__)
static __inline__ unsigned long long int arch_get_ticks(void)
{
	unsigned long long a, d;
	asm volatile("rdtsc" : "=a" (a), "=d" (d));
	return a | (d << 32);
}
#elif defined(__sun)
static __inline__ unsigned long long int arch_get_ticks(void)
{
	unsigned long long int x;
	/* gethrtime(3C) returns a signed long long but any way... */
	x = gethrtime();
	return x;
}
#else
#error "Unsupported arch, please report to frederik.deweerdt@gmail.com"
#endif /* arch_get_ticks */

/*
 * Definitely not the most efficient implementation out there, I'm open
 * to suggestions ;).
 */
static int first_call_to_dlock_printf = 1;
#define dlock_printf(format, ...) do { \
		char *p; \
		char prev = 'a'; \
		char buf[2048]; \
\
		if(first_call_to_dlock_printf) { \
			fprintf(dlock_log_file, "==%d== ", getpid()); \
			first_call_to_dlock_printf = 0; \
		} \
\
		sprintf(buf, format, ##__VA_ARGS__); \
		p = buf; \
		while (*p) { \
			putc(*p, dlock_log_file); \
			if (*p == '\n' && prev != '\n') \
				fprintf(dlock_log_file, "==%d== ", getpid()); \
			prev = *p; \
			p++; \
		} \
	} while(0)

/* Maximum number of lock/action pairs recorded in a lock chain */
#define MAX_LOCK_DEPTH 20
/* Number of different lock sequences recorded */
#define MAX_LOCK_SEQ 1024 * 1000
#define MAX_DEPENDS 10
/* Number of different locks known to the system */
#define MAX_KNOWN_LOCKS 1024 * 10
/* Length of the recorded name of a lock */
#define MAX_LOCK_NAME 255
#define MAX_FN_NAME 255

enum action_on_error {
	DLOCK_ACTION_IGNORE=	(1 << 0),
	DLOCK_ACTION_ABORT=	(1 << 1),
	DLOCK_ACTION_EXIT=	(1 << 2),
	DLOCK_ACTION_DUMP=	(1 << 3),
};
static unsigned int default_action = DLOCK_ACTION_ABORT | DLOCK_ACTION_DUMP;

#define dlock_error(action_flags, fmt, ...) \
	do { \
		struct dlock_trace *trace;\
\
		dlock_disable = 1; \
		dlock_printf(fmt, ##__VA_ARGS__); \
\
		dlock_printf("\n"); \
		trace = dlock_backtrace(); \
		free(trace); \
		if (action_flags & DLOCK_ACTION_DUMP) { \
			dlock_dump(); \
		} \
		if (action_flags & DLOCK_ACTION_IGNORE) { \
			break; \
		} \
		if (action_flags & DLOCK_ACTION_ABORT) { \
			abort(); \
		} \
		if (action_flags & DLOCK_ACTION_EXIT) { \
			exit(0); \
		} \
	} while (0)

static unsigned long machine_mhz = ~0UL;
static unsigned long calibrating_loop(void)
{
	struct timeval a;
	unsigned long long start, end;
	unsigned long mhz;

	gettimeofday(&a, NULL);
	start = arch_get_ticks();
	for (;;) {
		unsigned long usec;
		struct timeval b;
		gettimeofday(&b, NULL);
		usec = (b.tv_sec - a.tv_sec) * 1000000;
		usec += b.tv_usec - a.tv_usec;
		if (usec >= 1000000)
			break;
	}
	end = arch_get_ticks();
	mhz = end - start;
	return mhz;
}

struct dlock_trace *dlock_backtrace(void)
{
	void *array[4];
	struct dlock_trace *trace;
	size_t i;

	trace = malloc(sizeof(*trace));
	trace->size = ARRAY_SIZE(array);
	array[0] = __builtin_return_address(0);
	array[1] = __builtin_return_address(1);
	array[2] = __builtin_return_address(2);
	array[3] = __builtin_return_address(3);
	trace->syms = backtrace_symbols(array, trace->size);

	for (i = 0; i < trace->size; i++)
		dlock_printf("%s\n", trace->syms[i]);
	return trace;
}

static int dlock_disable = 0;

static void dlock_hijack_libpthread(void)
{
	void *lib_handle = NULL;

	if ( (lib_handle = dlopen("libpthread.so", RTLD_NOW)) == NULL) {
		if ( (lib_handle = dlopen("libpthread.so.0", RTLD_NOW)) == NULL) {
			dlock_error(DLOCK_ACTION_EXIT, "error loading libpthread!\n");
		}
	}

	HIJACK(lib_handle, pthread_mutex_init, "pthread_mutex_init");
	HIJACK(lib_handle, pthread_mutex_lock, "pthread_mutex_lock");
	HIJACK(lib_handle, pthread_mutex_trylock, "pthread_mutex_trylock");
	HIJACK(lib_handle, pthread_mutex_unlock, "pthread_mutex_unlock");

#if defined(__sun)
	/* Solaris stores the spinlock code in the libc */
	if ( (lib_handle = dlopen("libc.so", RTLD_NOW)) == NULL) {
		if ( (lib_handle = dlopen("libc.so.0", RTLD_NOW)) == NULL) {
			dlock_error(DLOCK_ACTION_EXIT, "error loading libpthread!\n");
		}
	}
#endif

	HIJACK(lib_handle, pthread_spin_init, "pthread_spin_init");
	HIJACK(lib_handle, pthread_spin_lock, "pthread_spin_lock");
	HIJACK(lib_handle, pthread_spin_trylock, "pthread_spin_trylock");
	HIJACK(lib_handle, pthread_spin_unlock, "pthread_spin_unlock");
}

struct thread_list {
	unsigned int tid;
	struct dlock_node *root;
	struct dlock_node *current_node;
	LIST_ENTRY(thread_list) entries;
};

struct sequence {
	unsigned long long max_held;
	unsigned long length;
	unsigned long cksum;
	unsigned long times;
	struct dlock_node *root;
};

struct lock_desc {
	dlock_lock_t *lock;
	char name[MAX_LOCK_NAME];
	char fn_name[MAX_FN_NAME];
	int ln;
	dlock_lock_t *depends_on[MAX_DEPENDS];
	enum lock_type type;
};

/* Holds the known lock_desc entries */
static struct lock_desc *lock_descs;
/* current index in the lock_descs array */
static unsigned int ld_index = 0;
/* Protects the lock_descs array */
static pthread_mutex_t descs_mutex = PTHREAD_MUTEX_INITIALIZER;

/* linked list containing the different threads running
   in the guest program */
static LIST_HEAD(llisthead, thread_list) lhead;
/* Protects the thread_list queue */
static pthread_mutex_t thread_list_mutex = PTHREAD_MUTEX_INITIALIZER;

/* Holds the sequences known to the system */
static struct sequence *tsequences;
/* Current index in the global sequences array */
static int index_tsequences = 0;
/* protectes tsequences and index_tsequences */
static pthread_mutex_t sequences_mutex = PTHREAD_MUTEX_INITIALIZER;

/* TLS variable containing the current's thread lock sequence */
static __thread struct thread_list *this_thread_lock_list;

/* Called by ld */
void _init(void)
{
	int flags;
	const char *str_flags = getenv("DLOCK_FLAGS");
	const char *str_on_error = getenv("DLOCK_ON_ERROR");
	const char *preload = getenv("LD_PRELOAD");

	lock_descs = calloc(MAX_KNOWN_LOCKS, sizeof(*lock_descs));
	if (!lock_descs) {
		dlock_printf("dlock: Allocate lock structures\n");
		exit(EXIT_FAILURE);
	}
	tsequences = calloc(MAX_LOCK_SEQ, sizeof(*lock_descs));
	if (!tsequences) {
		dlock_printf("dlock: Allocate lock structures\n");
		exit(EXIT_FAILURE);
	}

	if (!str_flags)
		flags = 0;
	else
		flags = strtol(str_flags, NULL, 0);

	if (preload && !str_flags) {
		flags = DLOCK_INIT_HANDLE_USR1;
	}

	if (flags & DLOCK_INIT_CALIBRATE)
		machine_mhz = calibrating_loop();

	if (flags & DLOCK_INIT_HANDLE_USR1)
		signal(SIGUSR1, (sighandler_t) dlock_dump);

	if (flags & DLOCK_INIT_HANDLE_USR1)
		signal(SIGUSR2, (sighandler_t) dlock_dump);

	if (flags & DLOCK_REGISTER_ATEXIT)
		atexit(dlock_dump);

	dlock_log_file = stderr;
	if (flags & DLOCK_LOG_FILE) {
		char *log_file_fmt = getenv("DLOCK_LOG_FILE");
		char log_file[FILENAME_MAX];
		FILE *f;

		sprintf(log_file, "%s.%d", log_file_fmt, getpid());

		f = fopen(log_file, "w+");
		if (!f)
			dlock_printf(
				"dlock: Cannot open %s file, falling back to stderr (remember to set DLOCK_LOG_FILE)\n",
				log_file_fmt ? log_file_fmt : "<null>");
		else
			dlock_log_file = f;
	}

	if (str_on_error)
		default_action = strtol(str_on_error, NULL, 0);

	dlock_hijack_libpthread();
}

static void create_queue(unsigned int tid)
{
	if (!this_thread_lock_list) {
		this_thread_lock_list = malloc(sizeof(struct thread_list));
		o_pthread_mutex_lock(&thread_list_mutex);
		LIST_INSERT_HEAD(&lhead, this_thread_lock_list, entries);
		o_pthread_mutex_unlock(&thread_list_mutex);
	}
	this_thread_lock_list->tid = tid;
	this_thread_lock_list->root = calloc(1, sizeof(struct dlock_node));
	this_thread_lock_list->current_node = this_thread_lock_list->root;
}

/**
 * @brief Returns the mutex, the current mutex depends on
 *
 * @param t
 *
 * @return the parent mutex or NULL if no parent mutex was found
 **/
static dlock_lock_t *get_previous_lock(struct thread_list *t)
{
	return t->current_node->parent->lock;
}

/**
 * @brief Add a dependecy relation between @master_mutex and the
 * mutex described by @desc
 *
 * @param desc The desc_mutex we want to mark as depending on @master_mutex
 * @param master_mutex The mutex on which the mutex described by @desc depends
 *
 **/
static void new_dependency(struct lock_desc *desc, dlock_lock_t *master_mutex)
{
	int i;

	/* skip to an empty stop in depends_on[] */
	for (i = 0; desc->depends_on[i] != master_mutex
	     && desc->depends_on[i] != NULL && i < MAX_DEPENDS; i++);

	/* sanity check */
	if (i >= MAX_KNOWN_LOCKS) {
		dlock_error(DLOCK_ACTION_ABORT,
			    "Your application uses too much mutexes, please disable libdlock.\n");
	}

	/* known dependency, don't add */
	if (desc->depends_on[i] == master_mutex)
		return;

	/* assign new dependency */
	desc->depends_on[i] = master_mutex;
}

/**
 * @brief Tests whenever @mutex depends on @probed_lock
 *
 * @param mutex
 * @param probed_lock
 *
 * @return 1 if @mutex depends on probed_lock, 0 otherwise
 **/
static int is_depending_on(dlock_lock_t *lock, dlock_lock_t *probed_lock)
{
	int i;
	struct lock_desc *desc;

	if (lock == probed_lock) {
		return 1;
	}

	desc = get_lock_desc(lock);

	for (i=0; desc->depends_on[i] != NULL && i < MAX_DEPENDS; i++) {
		if (desc->depends_on[i] == probed_lock) {
			return 1;
		}
		if (is_depending_on(desc->depends_on[i], probed_lock)) {
			return 1;
		}
	}
	return 0;
}

#define ALLOC_STEP 4
static void append_lock(struct thread_list *t, dlock_lock_t *lock)
{
	dlock_lock_t *previous;

	t->current_node->nb_children++;
	#if 1
	t->current_node->children = realloc(t->current_node->children,
		(t->current_node->nb_children) * sizeof(struct dlock_node));
	#else /* enable this if you have many childs for one lock */
	if ((t->current_node->nb_children % ALLOC_STEP) == 1) {
		t->current_node->children = realloc(t->current_node->children,
			(t->current_node->nb_children + ALLOC_STEP)
			* sizeof(struct dlock_node));
	}
	#endif
	CUR_CHILD(t) = calloc(1, sizeof(struct dlock_node));
	CUR_CHILD(t)->parent =  t->current_node;
	CUR_CHILD(t)->lock =  lock;
	CUR_CHILD(t)->lock_time = arch_get_ticks();
	CUR_CHILD(t)->stack = dlock_backtrace();

	t->current_node = CUR_CHILD(t);

	previous = get_previous_lock(t);

	if (previous) {
		/* let us know if the previous lock was already
		 * depending on mutex (deadlock?)*/
		if (is_depending_on(previous, lock)) {
			dlock_error(default_action,
				    "New dependency %p -> %p leads to deadlock\n",
				    previous, lock);
		}
		new_dependency(get_lock_desc(lock), previous);
	}
}

static int append_unlock(struct thread_list *t, dlock_lock_t *lock)
{
	/* check we're unlocking the current lock */
	if (t->current_node->lock != lock) {
		dlock_error(default_action,
			"Out of order unlocking %s locked -> unlocking %s\n",
			get_lock_desc(t->current_node->lock)->name,
			get_lock_desc(lock)->name);
	}

	t->current_node->unlock_time = arch_get_ticks();
	t->current_node = t->current_node->parent;

	/* is loop closed? */
	return (t->root->nb_children && FIRST_NODE(t)->lock == lock);
}

void __dlock_lock_init(dlock_lock_t *lock, const char *lock_name,
		       char *fn, int ln, enum lock_type type)
{
	int skip = 0;

	if (dlock_disable)
		return;

	/* Skip the leading ampersand for aesthetic purposes */
	if (lock_name[0] == '&') {
		skip = 1;
	}
	lock_descs[ld_index].lock = lock;
	strncpy(lock_descs[ld_index].name, lock_name + skip,
		MAX_LOCK_NAME);
	strncpy(lock_descs[ld_index].fn_name, fn, MAX_FN_NAME);
	lock_descs[ld_index].ln = ln;

	ld_index++;
	if (ld_index >= MAX_KNOWN_LOCKS) {
		dlock_error(DLOCK_ACTION_ABORT,
			    "Your application uses too much mutexes, please disable libdlock.\n");
	}
}

void dlock_lock_init(dlock_lock_t *lock, const char *lock_name,
		     char *fn, int ln, enum lock_type type)
{
	if (dlock_disable)
		return;

	o_pthread_mutex_lock(&descs_mutex);
	__dlock_lock_init(lock, lock_name, fn, ln, type);
	o_pthread_mutex_unlock(&descs_mutex);
}

void dlock_lock(dlock_lock_t *lock, unsigned int tid)
{
	if (dlock_disable)
		return;

	if (!this_thread_lock_list) {
		create_queue(tid);
	}
	append_lock(this_thread_lock_list, lock);
}

static void register_sequence(struct thread_list *t)
{
	unsigned long long mtime;

	o_pthread_mutex_lock(&sequences_mutex);

	tsequences[index_tsequences].cksum = cksum(t->root, 0);
	mtime = FIRST_NODE(t)->unlock_time - FIRST_NODE(t)->lock_time;
	tsequences[index_tsequences].max_held = mtime;
	tsequences[index_tsequences].times = 1;
	tsequences[index_tsequences].root = t->root;
	tsequences[index_tsequences].length = get_tree_depth(t->root, 0);
	index_tsequences++;
	if (index_tsequences >= MAX_LOCK_SEQ) {
		dlock_error(DLOCK_ACTION_ABORT,
			    "Too much locking sequences, please disable libdlock.\n");
	}

	o_pthread_mutex_unlock(&sequences_mutex);
	return;
}

/* Returns true if the sequence held by thread tid is
   already known to dlock */
static int is_registered_sequence(struct thread_list *t)
{
	int i, cksm, ret = 0;
	unsigned long long mtime, now;

	cksm = cksum(t->root, 0);
	o_pthread_mutex_lock(&sequences_mutex);
	for (i=0; i < index_tsequences; i++) {
		/* it _is_ registered */
		if (tsequences[i].cksum == cksm) {
			/*
			 * the max held time registered
			 * may have changed, update it
			 */
			now = arch_get_ticks();
			mtime = now - FIRST_NODE(t)->lock_time;
			if (tsequences[i].max_held < mtime)
				tsequences[i].max_held = mtime;
			tsequences[i].times++;
			ret = 1;
			goto out;
		}
	}
out:
	o_pthread_mutex_unlock(&sequences_mutex);
	return ret;
}

void dlock_unlock(dlock_lock_t *lock, unsigned int tid)
{
	if (dlock_disable)
		return;

	/* if not found: we started unlocking without a previous lock */
	if (!this_thread_lock_list) {
		dlock_error(DLOCK_ACTION_DUMP,
			    "Thread %d issued an unlock before any locking",
			    tid);
	}
	/* if loop closed */
	if (append_unlock(this_thread_lock_list, lock)) {
		/* do we already have such a sequence ? */
		if (!is_registered_sequence(this_thread_lock_list)) {
			register_sequence(this_thread_lock_list);
			/* reset the thread's queue */
			create_queue(tid);
		} else {
			/* simply drop the sequence,
			   we already know it */
			free_nodes(this_thread_lock_list->root);
			create_queue(tid);
		}
	}
}

static struct lock_desc *get_lock_desc(dlock_lock_t *lock)
{
	int i;
	char buf[16];
	struct lock_desc *ret;

	o_pthread_mutex_lock(&descs_mutex);
	for (i = 0; lock_descs[i].lock != NULL && i < MAX_KNOWN_LOCKS; i++) {
		if (lock == lock_descs[i].lock) {
			o_pthread_mutex_unlock(&descs_mutex);
			return &lock_descs[i];
		}
	}
	sprintf(buf, "%p", lock);
	__dlock_lock_init(lock, buf, "?", 0, UNKNOWN);
	ret = &lock_descs[ld_index - 1];
	o_pthread_mutex_unlock(&descs_mutex);

	return ret;
}

static void print_lock_tree(struct dlock_node *n, int depth)
{
	int i;
	for(i=0; i < depth - 1; i++) {
		if (i == depth - 2) {
			dlock_printf("\\----->\t");
		} else {
			dlock_printf("\t");
		}
	}
	/* special case, skip root */
	if (depth) {
		char *name = get_lock_desc(n->lock)->name;
		char *locked = "";

		/* display that the lock is held */
		if (n->unlock_time == 0)
			locked = " (locked)";

		dlock_printf("%s%s\n", name, locked);
		if (n->stack) {
			for (i = 0; i < n->stack->size; i++)
				dlock_printf("(lock time stack):%s\n", n->stack->syms[i]);
		}
	}
	for (i = 0; i < n->nb_children; i++) {
		print_lock_tree(n->children[i], depth+1);
	}
}

void dlock_dump(void)
{
	int i, j;

	dlock_printf("====================\n");
	dlock_printf("Registered lock sequences:\n");
	for (i = 0; i < index_tsequences; i++) {
		dlock_printf("held %llu ms, depth %lu, used %lu times\n",
		machine_mhz == ~0UL ? 0 : tsequences[i].max_held / (machine_mhz / 1000),
		tsequences[i].length, tsequences[i].times);

		print_lock_tree(tsequences[i].root, 0);
	}

	dlock_printf("====================\n");
	dlock_printf("Thread list:\n");
	struct thread_list *np;
	for (np = lhead.lh_first; np != NULL; np = np->entries.le_next) {
		dlock_printf("[%u]", np->tid);
		if (np->root->nb_children != 0) {
			dlock_printf("\n");
			print_lock_tree(np->root, 0);
		} else {
			dlock_printf(" no mutexes held\n");
		}
	}

	/* The mutexes need to be inited with pthread_mutex_init to be printed here */
	if (ld_index > 0) {

		dlock_printf("====================\n");

		dlock_printf("Mutexes known to dlock:\n");
		for (i = 0; i < ld_index; i++) {
			dlock_printf("\t%p %s=%s:%d\n",
				     lock_descs[i].lock,
				     get_lock_desc(lock_descs[i].lock)->name,
				     lock_descs[i].fn_name, lock_descs[i].ln);
			dlock_printf("\t\tdepends on:\n");
			for (j=0; lock_descs[i].depends_on[j] != NULL; j++) {
				dlock_printf("\t\t%p\n", lock_descs[i].depends_on[j]);
			}
		}
	}

	dlock_printf("====================\n");
}

static void print_dot_tree(struct dlock_node *n, int depth, int graph, char **labels)
{
	int i=0;
	/* special case, skip root */
	if (depth) {
		char *name = get_lock_desc(n->lock)->name;
		char *locked = "";
		char label[MAX_LOCK_NAME];

		/* display that the lock is held */
		if (n->unlock_time == 0)
			locked = " (locked)";

		if (depth > 1) {
			char *parent = get_lock_desc(n->parent->lock)->name;
			dlock_printf("\"%s%d\" -> \"%s%d\";\n", parent, graph, name, graph);
		} else if (n->nb_children == 0) {
			dlock_printf("\"%s%d\";\n", name, graph);
		}
		while (labels[i++]);
		sprintf(label, "\"%s%d\"[label = \"%s%s\"]", name, graph, name, locked);
		labels[i-1] = strdup(label);
	}
	for (i = 0; i < n->nb_children; i++) {
		print_dot_tree(n->children[i], depth+1, graph, labels);
	}
}

/**
 * @brief Dumps dot code to the log stream (see http://www.graphviz.org)
 *
 **/
void dlock_dump_dot(void)
{
	int i,j;
	fprintf(dlock_log_file, "digraph g {\n");
	for (i = 0; i < index_tsequences; i++) {
		char *labels[MAX_KNOWN_LOCKS];

		memset(labels, 0, sizeof(labels));

		fprintf(dlock_log_file, "subgraph cluster_%d {\n", i);
		fprintf(dlock_log_file, "color=black;\n");
		print_dot_tree(tsequences[i].root, 0, i, labels);

		j = 0;
		while (labels[j]) {
			fprintf(dlock_log_file, "%s\n", labels[j]);
			free(labels[j]);
			j++;
		}
		fprintf(dlock_log_file, "}\n");
	}
	fprintf(dlock_log_file, "}\n");
}
